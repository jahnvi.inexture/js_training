import Expenses from "./components/Expenses/Expenses";
import NewExpense from './components/NewExpense/NewExpense';

const App = () => {
  const expenses = [
    {
      title: "Book",
      amount: 200,
      date: new Date(2021, 4, 12),
    },
    {
      title: "Milk",
      amount: 20,
      date: new Date(2020, 3, 12),
    },
    {
      title: "Glass Bottle",
      amount: 340,
      date: new Date(2019, 1, 12),
    },
    {
      title: "Pen",
      amount: 20,
      date: new Date(2021, 9, 10),
    },
  ];

  return (
    <div>
      <NewExpense />
      <Expenses items={expenses} />
    </div>
  );
}

export default App;
